using System;
using System.Collections.Generic;

namespace Forum.Dal.Entities
{
    public class Conversation : BaseEntity
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? EditingDate { get; set; }

        public string ForumUserId { get; set; }

        public string CategoryId { get; set; }

        public ForumUser ForumUser { get; set; }

        public Category Category { get; set; }

        public IEnumerable<Post> Posts { get; set; }

        public IEnumerable<ConversationTag> ConversationTags { get; set; }
    }
}