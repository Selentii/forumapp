using System;
using System.Collections.Generic;

namespace Forum.Dal.Entities
{
    public class Post : BaseEntity
    {
        public string Content { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? EditingDate { get; set; }

        public string ReplyToId { get; set; }

        public string ForumUserId { get; set; }

        public string ConversationId { get; set; }

        public Post ReplyTo { get; set; }

        public IEnumerable<Post> Replies { get; set; }

        public ForumUser ForumUser { get; set; }

        public Conversation Conversation { get; set; }
    }
}