namespace Forum.Dal.Entities
{
    public class ConversationTag
    {
        public string ConversationId { get; set; }

        public string TagId { get; set; }

        public Conversation Conversation { get; set; }

        public Tag Tag { get; set; }
    }
}