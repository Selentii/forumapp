using System.Collections.Generic;

namespace Forum.Dal.Entities
{
    public class Tag : BaseEntity
    {
        public string Name { get; set; }

        public IEnumerable<ConversationTag> ConversationTags { get; set; }
    }
}