using System.Collections.Generic;

namespace Forum.Dal.Entities
{
    public class Category : BaseEntity
    {
        public string Name { get; set; } 

        public int Weight { get; set; }

        public IEnumerable<Conversation> Conversations { get; set; }
    }
}