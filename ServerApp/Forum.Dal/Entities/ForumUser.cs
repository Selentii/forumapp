using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Forum.Dal.Entities
{
    public class ForumUser : IdentityUser
    {
        public int Reputation { get; set; }

        public DateTime LastActiveDate { get; set; }

        public DateTime CreationDate { get; set; }

        public IEnumerable<Conversation> Conversations { get; set; }

        public IEnumerable<Post> Posts { get; set; }
    }
}