using Forum.Dal.Entities;
using Forum.Dal.EntityTypeConfigurations;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Forum.Dal
{
    public class ForumDbContext : IdentityDbContext<ForumUser>
    {
        public ForumDbContext(DbContextOptions<ForumDbContext> options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new CategoryEntityTypeConfiguration())
                .ApplyConfiguration(new ConversationEntityTypeConfiguration())
                .ApplyConfiguration(new PostEntityTypeConfiguration())
                .ApplyConfiguration(new TagEntityTypeConfiguration())
                .ApplyConfiguration(new ConversationTagEntityTagConfiguration());
        }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Conversation> Conversations { get; set; }

        public DbSet<ForumUser> ForumUsers { get; set; }

        public DbSet<Post> Posts { get; set; }

        public DbSet<Tag> Tags { get; set; }

        public DbSet<ConversationTag> ConversationTags { get; set; }
    }
}