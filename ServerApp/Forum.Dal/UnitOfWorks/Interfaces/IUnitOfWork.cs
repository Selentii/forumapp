using System.Threading.Tasks;
using Forum.Dal.Repositories.Interfaces;

namespace Forum.Dal.UnitOfWorks.Interfaces
{
    public interface IUnitOfWork
    {
        ICategoryRepository Categories { get; }

        IConversationRepository Conversations { get; }

        IConversationTagRepository ConversationTags { get; }

        IForumUserRepository ForumUsers { get; }

        IPostRepository Posts { get; }

        ITagRepository Tags { get; }

        Task SaveChangesAsync();
    }
}