using System;
using System.Threading.Tasks;
using Forum.Dal.Repositories;
using Forum.Dal.Repositories.Interfaces;
using Forum.Dal.UnitOfWorks.Interfaces;

namespace Forum.Dal.UnitOfWorks
{
    public class EFUnitOfWork : IUnitOfWork, IDisposable, IAsyncDisposable
    {
        private readonly ForumDbContext _context;

        private ICategoryRepository _categoryRepository;
        private IConversationRepository _conversationRepository;
        private IConversationTagRepository _conversationTagRepository;
        private IForumUserRepository _forumUserRepository;
        private IPostRepository _postRepository;
        private ITagRepository _tagRepository;

        public EFUnitOfWork(ForumDbContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            _context = context;
        }

        public ICategoryRepository Categories
            => _categoryRepository ??= new CategoryRepository(_context);

        public IConversationRepository Conversations
            => _conversationRepository ??= new ConversationRepository(_context);

        public IConversationTagRepository ConversationTags
            => _conversationTagRepository ??= new ConversationTagRepository(_context);

        public IForumUserRepository ForumUsers
            => _forumUserRepository ??= new ForumUserRepository(_context);

        public IPostRepository Posts
            => _postRepository ??= new PostRepository(_context);

        public ITagRepository Tags
            => _tagRepository ??= new TagRepository(_context);

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        public async ValueTask DisposeAsync()
        {
            await _context.DisposeAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}