﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Forum.Dal.Migrations
{
    public partial class AddedCreationDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Conversations_Categories_CategoryId",
                table: "Conversations");

            migrationBuilder.DropForeignKey(
                name: "FK_ConversationTag_Conversations_ConversationId",
                table: "ConversationTag");

            migrationBuilder.DropForeignKey(
                name: "FK_ConversationTag_Tags_TagId",
                table: "ConversationTag");

            migrationBuilder.DropForeignKey(
                name: "FK_Posts_Conversations_ConversationId",
                table: "Posts");

            migrationBuilder.DropForeignKey(
                name: "FK_Posts_Posts_ReplyToId",
                table: "Posts");

            migrationBuilder.DropIndex(
                name: "IX_Conversations_CategoryId",
                table: "Conversations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ConversationTag",
                table: "ConversationTag");

            migrationBuilder.RenameTable(
                name: "ConversationTag",
                newName: "ConversationTags");

            migrationBuilder.RenameIndex(
                name: "IX_ConversationTag_TagId",
                table: "ConversationTags",
                newName: "IX_ConversationTags_TagId");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationDate",
                table: "Posts",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Conversations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ConversationTags",
                table: "ConversationTags",
                columns: new[] { "ConversationId", "TagId" });

            migrationBuilder.CreateIndex(
                name: "IX_Conversations_CategoryId",
                table: "Conversations",
                column: "CategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Conversations_Categories_CategoryId",
                table: "Conversations",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_ConversationTags_Conversations_ConversationId",
                table: "ConversationTags",
                column: "ConversationId",
                principalTable: "Conversations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ConversationTags_Tags_TagId",
                table: "ConversationTags",
                column: "TagId",
                principalTable: "Tags",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_Conversations_ConversationId",
                table: "Posts",
                column: "ConversationId",
                principalTable: "Conversations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_Posts_ReplyToId",
                table: "Posts",
                column: "ReplyToId",
                principalTable: "Posts",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Conversations_Categories_CategoryId",
                table: "Conversations");

            migrationBuilder.DropForeignKey(
                name: "FK_ConversationTags_Conversations_ConversationId",
                table: "ConversationTags");

            migrationBuilder.DropForeignKey(
                name: "FK_ConversationTags_Tags_TagId",
                table: "ConversationTags");

            migrationBuilder.DropForeignKey(
                name: "FK_Posts_Conversations_ConversationId",
                table: "Posts");

            migrationBuilder.DropForeignKey(
                name: "FK_Posts_Posts_ReplyToId",
                table: "Posts");

            migrationBuilder.DropIndex(
                name: "IX_Conversations_CategoryId",
                table: "Conversations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ConversationTags",
                table: "ConversationTags");

            migrationBuilder.DropColumn(
                name: "CreationDate",
                table: "Posts");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Conversations");

            migrationBuilder.RenameTable(
                name: "ConversationTags",
                newName: "ConversationTag");

            migrationBuilder.RenameIndex(
                name: "IX_ConversationTags_TagId",
                table: "ConversationTag",
                newName: "IX_ConversationTag_TagId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ConversationTag",
                table: "ConversationTag",
                columns: new[] { "ConversationId", "TagId" });

            migrationBuilder.CreateIndex(
                name: "IX_Conversations_CategoryId",
                table: "Conversations",
                column: "CategoryId",
                unique: true,
                filter: "[CategoryId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Conversations_Categories_CategoryId",
                table: "Conversations",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ConversationTag_Conversations_ConversationId",
                table: "ConversationTag",
                column: "ConversationId",
                principalTable: "Conversations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ConversationTag_Tags_TagId",
                table: "ConversationTag",
                column: "TagId",
                principalTable: "Tags",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_Conversations_ConversationId",
                table: "Posts",
                column: "ConversationId",
                principalTable: "Conversations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_Posts_ReplyToId",
                table: "Posts",
                column: "ReplyToId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
