using System.Collections.Generic;
using System.Linq;

namespace Forum.Dal.Infrastructure
{
    public class QueryResult
    {
        private List<QueryError> _errors = new List<QueryError>();

        private static readonly QueryResult _success =
            new QueryResult { Succeeded = true };

        public bool Succeeded { get; private set; }

        public IEnumerable<QueryError> Errors => _errors;

        public static QueryResult Success => _success;

        public static QueryResult Failed(params QueryError[] errors)
        {
            var result = new QueryResult { Succeeded = false };
            if (errors != null)
                result._errors.AddRange(errors);

            return result;
        }

        public override string ToString()
        {
            if (Succeeded)
                return "Succeeded";

            return string.Format("{0} : {1}", "Failed", string.Join(",", Errors.Select(x => x.Code)));
        }
    }
}