namespace Forum.Dal.Infrastructure
{
    public class QueryError
    {
        public string Code { get; set; }

        public string Description { get; set; }
    }
}