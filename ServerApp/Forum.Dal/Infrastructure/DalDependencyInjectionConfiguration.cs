using Forum.Dal.Entities;
using Forum.Dal.UnitOfWorks;
using Forum.Dal.UnitOfWorks.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Forum.Dal.Infrastructure
{
    public static class DalDependencyInjectionConfiguration
    {
        public static IServiceCollection ConfigureDal(this IServiceCollection services,
            IConfiguration configuration)
        {
            string connectionString = configuration["ConnectionStrings:DefaultConnection"];

            services.AddDbContext<ForumDbContext>(options =>
                options.UseSqlServer(connectionString));

            services.AddIdentityCore<ForumUser>()
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<ForumDbContext>();

            services.AddTransient<IUnitOfWork, EFUnitOfWork>();

            return services;
        }
    }
}