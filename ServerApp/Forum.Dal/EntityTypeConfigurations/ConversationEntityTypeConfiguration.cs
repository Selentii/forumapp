using Forum.Dal.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Forum.Dal.EntityTypeConfigurations
{
    public class ConversationEntityTypeConfiguration : IEntityTypeConfiguration<Conversation>
    {
        public void Configure(EntityTypeBuilder<Conversation> builder)
        {
            builder.Property(c => c.Title)
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(c => c.Description)
                .HasMaxLength(1000);

            builder.HasMany(c => c.Posts)
                .WithOne(p => p.Conversation)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasMany(c => c.ConversationTags)
                .WithOne(ct => ct.Conversation)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}