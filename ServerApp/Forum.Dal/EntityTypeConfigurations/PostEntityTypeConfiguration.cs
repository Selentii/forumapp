using Forum.Dal.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Forum.Dal.EntityTypeConfigurations
{
    public class PostEntityTypeConfiguration : IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.HasOne(p => p.ReplyTo)
                .WithMany(p => p.Replies)
                .HasForeignKey(p => p.ReplyToId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.Property(p => p.Content)
                .HasMaxLength(1000)
                .IsRequired();
        }
    }
}