using Forum.Dal.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Forum.Dal.EntityTypeConfigurations
{
    public class CategoryEntityTypeConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.Property(c => c.Name)
                .HasMaxLength(30)
                .IsRequired();

            builder.HasMany(c => c.Conversations)
                .WithOne(c => c.Category)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}