using Forum.Dal.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Forum.Dal.EntityTypeConfigurations
{
    public class ConversationTagEntityTagConfiguration : IEntityTypeConfiguration<ConversationTag>
    {
        public void Configure(EntityTypeBuilder<ConversationTag> builder)
        {
            builder.HasKey(ct => new { ct.ConversationId, ct.TagId });
        }
    }
}