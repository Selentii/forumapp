using System;
using System.Threading.Tasks;
using Forum.Dal.Entities;
using Forum.Dal.Infrastructure;
using Forum.Dal.Repositories.Interfaces;

namespace Forum.Dal.Repositories
{
    public class ConversationTagRepository : IConversationTagRepository
    {
        private readonly ForumDbContext _context;

        public ConversationTagRepository(ForumDbContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            _context = context;
        }

        public async Task<QueryResult> AddAsync(ConversationTag conversationTag)
        {
            if (conversationTag == null)
                throw new ArgumentNullException(nameof(conversationTag));

            await _context.ConversationTags.AddAsync(conversationTag);
            return QueryResult.Success;
        }

        public async Task<QueryResult> DeleteAsync(ConversationTag conversationTag)
        {
            if (conversationTag == null)
                throw new ArgumentNullException(nameof(conversationTag));

            var ct = await _context.ConversationTags
                .FindAsync(new { conversationTag.ConversationId, conversationTag.TagId });
            if (ct == null)
                return QueryResult.Failed(GetNotFoundError());

            _context.ConversationTags.Remove(ct);
            return QueryResult.Success;
        }

        private QueryError GetNotFoundError()
        {
            return new QueryError
            {
                Code = "NotFound",
                Description = "ConversationTag with specified primary key is not found"
            };
        }
    }
}