using System;
using System.Threading.Tasks;
using Forum.Dal.Entities;
using Forum.Dal.Pagination;
using Forum.Dal.Repositories.Interfaces;

namespace Forum.Dal.Repositories
{
    public class ForumUserRepository : IForumUserRepository
    {
        private readonly ForumDbContext _context;

        public ForumUserRepository(ForumDbContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            _context = context;
        }

        public async Task<ForumUser> GetAsync(string id)
        {
            return await _context.Users.FindAsync(id);
        }

        public async Task<PaginatedList<ForumUser>> GetAllAsync(int pageIndex, int pageSize)
        {
            var query = _context.Users;
            return await PaginatedList<ForumUser>.CreateAsync(query, pageIndex, pageSize);
        }
    }
}