using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Forum.Dal.Entities;
using Forum.Dal.Infrastructure;
using Forum.Dal.Pagination;

namespace Forum.Dal.Repositories.Interfaces
{
    public interface IPostRepository
    {
        Task<Post> GetAsync(string id);

        Task<PaginatedList<Post>> GetAllAsync(int pageIndex, int pageSize);

        Task<PaginatedList<Post>> GetByPredicateAsync(
            Expression<Func<Post, bool>> predicate, int pageIndex, int pageSize);
        
        Task<QueryResult> AddAsync(Post post);

        Task<QueryResult> EditAsync(Post post);

        Task<QueryResult> DeleteAsync(Post post);
    }
}