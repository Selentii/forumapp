using System.Threading.Tasks;
using Forum.Dal.Entities;
using Forum.Dal.Infrastructure;
using Forum.Dal.Pagination;

namespace Forum.Dal.Repositories.Interfaces
{
    public interface ICategoryRepository
    {
        Task<Category> GetAsync(string id);

        Task<PaginatedList<Category>> GetAllAsync(int pageIndex, int pageSize);

        Task<QueryResult> AddAsync(Category category);

        Task<QueryResult> EditAsync(Category category);

        Task<QueryResult> DeleteAsync(Category category);
    }
}