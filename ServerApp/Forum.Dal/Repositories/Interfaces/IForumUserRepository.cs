using System.Threading.Tasks;
using Forum.Dal.Entities;
using Forum.Dal.Pagination;

namespace Forum.Dal.Repositories.Interfaces
{
    public interface IForumUserRepository
    {
        Task<ForumUser> GetAsync(string id);
        
        Task<PaginatedList<ForumUser>> GetAllAsync(int pageIndex, int pageSize);
    }
}