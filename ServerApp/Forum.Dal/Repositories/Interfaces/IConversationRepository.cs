using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Forum.Dal.Entities;
using Forum.Dal.Infrastructure;
using Forum.Dal.Pagination;

namespace Forum.Dal.Repositories.Interfaces
{
    public interface IConversationRepository
    {
        Task<Conversation> GetAsync(string id);

        Task<PaginatedList<Conversation>> GetAllAsync(int pageIndex, int pageSize);

        Task<PaginatedList<Conversation>> GetByPredicateAsync(
            Expression<Func<Conversation, bool>> predicate, int pageIndex, int pageSize);

        Task<QueryResult> AddAsync(Conversation conversation);

        Task<QueryResult> EditAsync(Conversation conversation);

        Task<QueryResult> DeleteAsync(Conversation conversation);
    }
}