using System.Threading.Tasks;
using Forum.Dal.Entities;
using Forum.Dal.Infrastructure;

namespace Forum.Dal.Repositories.Interfaces
{
    public interface IConversationTagRepository
    {
        Task<QueryResult> AddAsync(ConversationTag conversationTag);

        Task<QueryResult> DeleteAsync(ConversationTag conversationTag);
    }
}