using System.Threading.Tasks;
using Forum.Dal.Entities;
using Forum.Dal.Infrastructure;
using Forum.Dal.Pagination;

namespace Forum.Dal.Repositories.Interfaces
{
    public interface ITagRepository
    {
        Task<Tag> GetAsync(string id);

        Task<PaginatedList<Tag>> GetAllAsync(int pageIndex, int pageSize);

        Task<QueryResult> AddAsync(Tag tag);

        Task<QueryResult> EditAsync(Tag tag);

        Task<QueryResult> DeleteAsync(Tag tag);
    }
}