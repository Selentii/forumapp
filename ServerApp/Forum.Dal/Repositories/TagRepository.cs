using System;
using System.Threading.Tasks;
using Forum.Dal.Entities;
using Forum.Dal.Infrastructure;
using Forum.Dal.Pagination;
using Forum.Dal.Repositories.Interfaces;

namespace Forum.Dal.Repositories
{
    public class TagRepository : ITagRepository
    {
        private readonly ForumDbContext _context;

        public TagRepository(ForumDbContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            _context = context;
        }

        public async Task<Tag> GetAsync(string id)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            return await _context.Tags.FindAsync(id);
        }

        public async Task<PaginatedList<Tag>> GetAllAsync(int pageIndex, int pageSize)
        {
            var query = _context.Tags;
            return await PaginatedList<Tag>
                .CreateAsync(query, pageIndex, pageSize);
        }

        public async Task<QueryResult> AddAsync(Tag tag)
        {
            if (tag == null)
                throw new ArgumentNullException(nameof(tag));

            await _context.Tags.AddAsync(tag);
            return QueryResult.Success;
        }

        public async Task<QueryResult> EditAsync(Tag tag)
        {
            if (tag == null)
                throw new ArgumentNullException(nameof(tag));

            var t = await _context.Tags.FindAsync(tag.Id);
            if (t == null)
                return QueryResult.Failed(GetNotFoundError());

            if (!string.IsNullOrWhiteSpace(t.Name))
                t.Name = tag.Name;

            return QueryResult.Success;
        }

        public async Task<QueryResult> DeleteAsync(Tag tag)
        {
            if (tag == null)
                throw new ArgumentNullException(nameof(tag));

            var t = await _context.Tags.FindAsync(tag.Id);
            if (t == null)
                return QueryResult.Failed(GetNotFoundError());

            _context.Tags.Remove(t);
            return QueryResult.Success;
        }

        private QueryError GetNotFoundError()
        {
            return new QueryError
            {
                Code = "NotFound",
                Description = "Tag with specified primary key is not found"
            };
        }
    }
}