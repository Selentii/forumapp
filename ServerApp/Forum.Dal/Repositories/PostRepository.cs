using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Forum.Dal.Entities;
using Forum.Dal.Infrastructure;
using Forum.Dal.Pagination;
using Forum.Dal.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Forum.Dal.Repositories
{
    public class PostRepository : IPostRepository
    {
        private readonly ForumDbContext _context;

        public PostRepository(ForumDbContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            _context = context;
        }

        public async Task<Post> GetAsync(string id)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            var post = await _context.Posts
                .Include(p => p.ReplyTo)
                .Include(p => p.ForumUser)
                .FirstOrDefaultAsync(p => p.Id == id);

            if (post.ReplyTo != null)
                post.ReplyTo.Replies = null;
            if (post.ForumUser != null)
                post.ForumUser.Posts = null;

            return post;
        }

        public async Task<PaginatedList<Post>> GetAllAsync(int pageIndex, int pageSize)
        {
            var query = _context.Posts
                .Include(p => p.ReplyTo)
                .Include(p => p.ForumUser);

            await query.ForEachAsync(p =>
            {
                if (p.ReplyTo != null)
                    p.ReplyTo.Replies = null;
                if (p.ForumUser != null)
                    p.ForumUser.Posts = null;
            });

            return await PaginatedList<Post>
                .CreateAsync(query, pageIndex, pageSize);
        }

        public async Task<PaginatedList<Post>> GetByPredicateAsync(
            Expression<Func<Post, bool>> predicate, int pageIndex, int pageSize)
        {
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));
            if (pageIndex < 1)
                throw new ArgumentOutOfRangeException(nameof(pageIndex));
            if (pageSize < 1)
                throw new ArgumentOutOfRangeException(nameof(pageSize));

            var query = _context.Posts
                .Where(predicate)
                .Include(p => p.ReplyTo)
                .Include(p => p.ForumUser);

            await query.ForEachAsync(p =>
            {
                if (p.ReplyTo != null)
                    p.ReplyTo.Replies = null;
                if (p.ForumUser != null)
                    p.ForumUser.Posts = null;
            });

            return await PaginatedList<Post>
                .CreateAsync(query, pageIndex, pageSize);
        }

        public async Task<QueryResult> AddAsync(Post post)
        {
            if (post == null)
                throw new ArgumentNullException(nameof(post));

            await _context.Posts.AddAsync(post);
            return QueryResult.Success;
        }

        public async Task<QueryResult> EditAsync(Post post)
        {
            if (post == null)
                throw new ArgumentNullException(nameof(post));

            var p = await _context.Posts.FindAsync(post.Id);
            if (p == null)
                return QueryResult.Failed(GetNotFoundError());

            if (!string.IsNullOrEmpty(post.Content))
                p.Content = post.Content;

            return QueryResult.Success;
        }

        public async Task<QueryResult> DeleteAsync(Post post)
        {
            if (post == null)
                throw new ArgumentNullException(nameof(post));

            var p = await _context.Posts.FindAsync(post.Id);
            if (p == null)
                return QueryResult.Failed(GetNotFoundError());

            _context.Posts.Remove(p);
            return QueryResult.Success;
        }

        private QueryError GetNotFoundError()
        {
            return new QueryError
            {
                Code = "NotFound",
                Description = "Post with specified primary key is not found"
            };
        }
    }
}