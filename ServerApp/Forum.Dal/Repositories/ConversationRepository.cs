using System;
using System.Linq;
using System.Threading.Tasks;
using Forum.Dal.Entities;
using Forum.Dal.Infrastructure;
using Forum.Dal.Pagination;
using Forum.Dal.Repositories.Interfaces;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace Forum.Dal.Repositories
{
    public class ConversationRepository : IConversationRepository
    {
        private readonly ForumDbContext _context;

        public ConversationRepository(ForumDbContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            _context = context;
        }

        public async Task<Conversation> GetAsync(string id)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            var conversation = await _context.Conversations
                .Include(c => c.ForumUser)
                .Include(c => c.Category)
                .Include(c => c.ConversationTags)
                .ThenInclude(ct => ct.Tag)
                .FirstOrDefaultAsync(c => c.Id == id);

            if (conversation.ForumUser != null)
                conversation.ForumUser.Conversations = null;
            if (conversation.Category != null)
                conversation.Category.Conversations = null;

            if (conversation.ConversationTags != null)
            {
                foreach (var ct in conversation.ConversationTags)
                {
                    ct.Conversation = null;
                    ct.Tag.ConversationTags = null;
                }
            }

            return conversation;
        }

        public async Task<PaginatedList<Conversation>> GetAllAsync(int pageIndex, int pageSize)
        {
            if (pageIndex < 1)
                throw new ArgumentOutOfRangeException(nameof(pageIndex));
            if (pageSize < 1)
                throw new ArgumentOutOfRangeException(nameof(pageSize));

            var query = _context.Conversations
                .Include(c => c.ForumUser)
                .Include(c => c.Category)
                .Include(c => c.ConversationTags)
                .ThenInclude(ct => ct.Tag);

            await query.ForEachAsync(c =>
            {
                if (c.ForumUser != null)
                    c.ForumUser.Conversations = null;
                if (c.Category != null)
                    c.Category.Conversations = null;

                if (c.ConversationTags != null)
                {
                    foreach (var ct in c.ConversationTags)
                    {
                        ct.Conversation = null;
                        ct.Tag.ConversationTags = null;
                    }
                }
            });

            return await PaginatedList<Conversation>
                .CreateAsync(query, pageIndex, pageSize);
        }

        public async Task<PaginatedList<Conversation>> GetByPredicateAsync(
            Expression<Func<Conversation, bool>> predicate, int pageIndex, int pageSize)
        {
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));
            if (pageIndex < 1)
                throw new ArgumentOutOfRangeException(nameof(pageIndex));
            if (pageSize < 1)
                throw new ArgumentOutOfRangeException(nameof(pageSize));

            var query = _context.Conversations
                .Where(predicate)
                .Include(c => c.ForumUser)
                .Include(c => c.Category)
                .Include(c => c.ConversationTags)
                .ThenInclude(ct => ct.Tag);

            await query.ForEachAsync(c =>
            {
                if (c.ForumUser != null)
                    c.ForumUser.Conversations = null;
                if (c.Category != null)
                    c.Category.Conversations = null;

                if (c.ConversationTags != null)
                {
                    foreach (var ct in c.ConversationTags)
                    {
                        ct.Conversation = null;
                        ct.Tag.ConversationTags = null;
                    }
                }
            });

            return await PaginatedList<Conversation>
                .CreateAsync(query, pageIndex, pageSize);
        }

        public async Task<QueryResult> AddAsync(Conversation conversation)
        {
            if (conversation == null)
                throw new ArgumentNullException(nameof(conversation));

            await _context.Conversations.AddAsync(conversation);
            return QueryResult.Success;
        }

        public async Task<QueryResult> EditAsync(Conversation conversation)
        {
            if (conversation == null)
                throw new ArgumentNullException(nameof(conversation));

            var c = await _context.Conversations.FindAsync(conversation.Id);
            if (c == null)
                return QueryResult.Failed(GetNotFoundError());

            if (!string.IsNullOrEmpty(conversation.Title))
                c.Title = conversation.Title;
            if (!string.IsNullOrEmpty(conversation.Description))
                c.Description = conversation.Description;
            if (!string.IsNullOrEmpty(conversation.CategoryId))
                c.CategoryId = conversation.CategoryId;

            return QueryResult.Success;
        }

        public async Task<QueryResult> DeleteAsync(Conversation conversation)
        {
            if (conversation == null)
                throw new ArgumentNullException(nameof(conversation));

            var c = await _context.Conversations.FindAsync(conversation.Id);
            if (c == null)
                return QueryResult.Failed(GetNotFoundError());

            _context.Conversations.Remove(c);
            return QueryResult.Success;
        }

        private QueryError GetNotFoundError()
        {
            return new QueryError
            {
                Code = "NotFound",
                Description = "Conversation with specified primary key is not found"
            };
        }
    }
}