using System;
using System.Threading.Tasks;
using Forum.Dal.Entities;
using Forum.Dal.Infrastructure;
using Forum.Dal.Pagination;
using Forum.Dal.Repositories.Interfaces;

namespace Forum.Dal.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly ForumDbContext _context;
        
        public CategoryRepository(ForumDbContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            _context = context;
        }

        public async Task<Category> GetAsync(string id)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));
                
            return await _context.Categories.FindAsync(id);
        }

        public async Task<PaginatedList<Category>> GetAllAsync(int pageIndex, int pageSize)
        {
            var query = _context.Categories;
            return await PaginatedList<Category>
                .CreateAsync(query, pageIndex, pageSize);
        }

        public async Task<QueryResult> AddAsync(Category category)
        {
            if (category == null)
                throw new ArgumentNullException(nameof(category));

            await _context.Categories.AddAsync(category);
            return QueryResult.Success;
        }

        public async Task<QueryResult> EditAsync(Category category)
        {
            if (category == null)
                throw new ArgumentNullException(nameof(category));

            var c = await _context.Categories.FindAsync(category.Id);
            if (c == null)
                return QueryResult.Failed(GetNotFoundError());

            if (!string.IsNullOrEmpty(category.Name))
                c.Name = category.Name;
            if (category.Weight != 0)
                c.Weight = category.Weight;

            return QueryResult.Success;
        }

        public async Task<QueryResult> DeleteAsync(Category category)
        {
            if (category == null)
                throw new ArgumentNullException(nameof(category));

            var c = await _context.Categories.FindAsync(category.Id);
            if (c == null)
                return QueryResult.Failed(GetNotFoundError());

            _context.Categories.Remove(c);
            return QueryResult.Success;
        }

        private QueryError GetNotFoundError()
        {
            return new QueryError
            {
                Code = "NotFound",
                Description = "Category with specified primary key is not found"
            };
        }
    }
}