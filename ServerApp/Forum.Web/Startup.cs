using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Forum.Bll.Infrastructure;
using Forum.Web.Extensions;
using Forum.Web.Middlewares;

namespace Forum.Web
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.ConfigureBll(_configuration);

            services.AddControllers()
                .AddJsonOptions(options
                    => options.JsonSerializerOptions.IgnoreNullValues = true);

            services.ConfigureSwagger();

            services.Configure<AuthOptions>(_configuration.GetSection("AuthOptions"));

            services.ConfigureAuthentication(_configuration);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDataSeeder seeder)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseMiddleware<ApiExceptionHandlingMiddleware>();
            }
            app.UseMiddleware<ApiExceptionHandlingMiddleware>();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json",
                    "Forum API");
            });

            if ((_configuration["INITDB"] ?? "false") == "true")
            {
                seeder.SeedDataAsync().Wait();
            }
        }
    }
}
