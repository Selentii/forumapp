using System;
using System.Threading.Tasks;
using Forum.Bll.DTOs;
using Forum.Bll.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Forum.Web.Controllers
{
    [ApiController]
    [Route("api/authenticate/")]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authenticateService;

        public AuthController(IAuthService authenticationService)
        {
            if (authenticationService == null)
                throw new ArgumentNullException(nameof(authenticationService));

            _authenticateService = authenticationService;
        }

        [HttpPost("login")]
        public async Task<IActionResult> LoginAsync(LoginDto loginDto)
        {
            var token = await _authenticateService.LoginAsync(loginDto);

            if (token == null)
                return Unauthorized();

            return Ok(token);
        }

        [HttpPost("register")]
        public async Task<IActionResult> RegisterAsync(RegisterDto registerDto)
        {
            var user = await _authenticateService.RegisterAsync(registerDto);

            if (user == null)
                return BadRequest();

            return CreatedAtAction("GetUser", 
                new { controller = "User", id = user.Id }, user);
        }
    }
}