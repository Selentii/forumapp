using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Forum.Bll.DTOs;
using Forum.Bll.Pagination;
using Forum.Bll.Services.Interfaces;
using Forum.Web.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Forum.Web.Controllers
{
    [ApiController]
    [Route("api/conversations")]
    [TypeFilter(typeof(LastActiveDateActionFilter))]
    public class ConversationController : ControllerBase
    {
        private readonly IConversationService _conversationService;

        private readonly IAuthService _authService;

        public ConversationController(IConversationService conversationService, IAuthService authService)
        {
            if (conversationService == null)
                throw new ArgumentNullException(nameof(conversationService));
            if (authService == null)
                throw new ArgumentNullException(nameof(authService));

            _conversationService = conversationService;
            _authService = authService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ConversationDto>> GetConversation(string id)
        {
            if (string.IsNullOrEmpty(id))
                return BadRequest("The id parameter is required.");

            var conversation = await _conversationService.GetConversationAsync(id);
            if (conversation == null)
                return NotFound();

            return conversation;
        }

        [HttpGet]
        public async Task<ActionResult<PaginatedListDto<ConversationDto>>> GetConversations(
            int pageIndex, int pageSize, string categoryId = null, string title = null, [FromQuery] string[] tagNames = null)
        {
            if (pageIndex < 1)
                return BadRequest("The pageIndex parameter must be at least 1.");
            if (pageSize < 1)
                return BadRequest("The pageSize parameter must be at least 1.");

            var conversations = await _conversationService
                .GetConversationsAsync(pageIndex, pageSize, categoryId, title, tagNames);
            return conversations;
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> AddConversation(AddConversationDto addConversationDto)
        {
            if (ModelState.IsValid)
            {
                var userId = HttpContext.User?.FindFirst(ClaimTypes.NameIdentifier).Value;
                if (userId == null)
                    return StatusCode(401);
                await _conversationService.AddConversationAsync(addConversationDto, userId);
                return Ok();
            }
            return BadRequest(ModelState);
        }

        [HttpPut]
        [Authorize]
        public async Task<IActionResult> EditConversation(EditConversationDto editConversationDto)
        {
            if (ModelState.IsValid)
            {
                var userId = HttpContext.User?.FindFirst(ClaimTypes.NameIdentifier).Value;
                if (userId == null)
                    return StatusCode(401);

                var conversation = await _conversationService.GetConversationAsync(editConversationDto.Id);
                if (conversation == null)
                    return NotFound();

                if (userId != conversation.ForumUserId &&
                    await _authService.IsInRole(userId, "Registred"))
                    return StatusCode(403);

                await _conversationService.EditConversationAsync(editConversationDto);
                return Ok();
            }
            return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteConversation(string id)
        {
            if (id == null)
                return BadRequest("The id parameter is required.");

            var userId = HttpContext.User?.FindFirst(ClaimTypes.NameIdentifier).Value;
            if (userId == null)
                return StatusCode(401);

            var conversation = await _conversationService.GetConversationAsync(id);
            if (conversation == null)
                return NotFound();

            if (userId != conversation.ForumUserId &&
                await _authService.IsInRole(userId, "Registred"))
                return StatusCode(403);

            await _conversationService.DeleteConversationAsync(id);
            return Ok();
        }

        [HttpPost("tag")]
        [Authorize]
        public async Task<IActionResult> TagConversation(ConversationTagDto conversationTagDto)
        {
            if (ModelState.IsValid)
            {
                var userId = HttpContext.User?.FindFirst(ClaimTypes.NameIdentifier).Value;
                if (userId == null)
                    return StatusCode(401);

                var conversation = await _conversationService.GetConversationAsync(conversationTagDto.ConversationId);
                if (conversation == null)
                    return NotFound();

                if (userId != conversation.ForumUserId &&
                    await _authService.IsInRole(userId, "Registred"))
                    return StatusCode(403);

                await _conversationService.TagConversationAsync(conversationTagDto);
                return Ok();
            }
            return BadRequest(ModelState);
        }

        [HttpDelete("untag")]
        [Authorize]
        public async Task<IActionResult> UnTagConversation(ConversationTagDto conversationTagDto)
        {
            if (ModelState.IsValid)
            {
                var userId = HttpContext.User?.FindFirst(ClaimTypes.NameIdentifier).Value;
                if (userId == null)
                    return StatusCode(401);

                var conversation = await _conversationService.GetConversationAsync(conversationTagDto.ConversationId);
                if (conversation == null)
                    return NotFound();

                if (userId != conversation.ForumUserId &&
                    await _authService.IsInRole(userId, "Registred"))
                    return StatusCode(403);

                await _conversationService.UnTagConversationAsync(conversationTagDto);
                return Ok();
            }
            return BadRequest(ModelState);
        }
    }
}