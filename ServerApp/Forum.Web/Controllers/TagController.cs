using System;
using System.Threading.Tasks;
using Forum.Bll.DTOs;
using Forum.Bll.Pagination;
using Forum.Bll.Services.Interfaces;
using Forum.Web.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Forum.Web.Controllers
{
    [ApiController]
    [Route("api/tags")]
    [TypeFilter(typeof(LastActiveDateActionFilter))]
    public class TagController : ControllerBase
    {
        private readonly ITagService _tagService;

        public TagController(ITagService tagService)
        {
            if (tagService == null)
                throw new ArgumentNullException(nameof(tagService));

            _tagService = tagService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TagDto>> GetTag(string id)
        {
            if (string.IsNullOrEmpty(id))
                return BadRequest("The id parameter is required.");

            var tag = await _tagService.GetTagAsync(id);
            if (tag == null)
                return NotFound();

            return tag;
        }

        [HttpGet]
        public async Task<ActionResult<PaginatedListDto<TagDto>>> GetTags(int pageIndex, int pageSize)
        {
            if (pageIndex < 1)
                return BadRequest("The pageIndex parameter must be at least 1.");
            if (pageSize < 1)
                return BadRequest("The pageSize parameter must be at least 1.");

            var tags = await _tagService.GetTagsAsync(pageIndex, pageSize);
            return tags;
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> AddTag(AddTagDto addTagDto)
        {
            if (ModelState.IsValid)
            {
                await _tagService.AddTagAsync(addTagDto);
                return Ok();
            }
            return BadRequest(ModelState);
        }

        [HttpPut]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> EditTag(EditTagDto editTagDto)
        {
            if (ModelState.IsValid)
            {
                await _tagService.EditTagAsync(editTagDto);
                return Ok();
            }
            return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> DeleteTag(string id)
        {
            if (id == null)
                return BadRequest("The id parameter is required.");

            await _tagService.DeleteTagAsync(id);
            return Ok();
        }
    }
}