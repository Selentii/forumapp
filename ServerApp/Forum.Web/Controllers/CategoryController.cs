using System;
using System.Threading.Tasks;
using Forum.Bll.DTOs;
using Forum.Bll.Pagination;
using Forum.Bll.Services.Interfaces;
using Forum.Web.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Forum.Web.Controllers
{
    [ApiController]
    [Route("api/categories")]
    [TypeFilter(typeof(LastActiveDateActionFilter))]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            if (categoryService == null)
                throw new ArgumentNullException(nameof(categoryService));

            _categoryService = categoryService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<CategoryDto>> GetCategory(string id)
        {
            if (string.IsNullOrEmpty(id))
                return BadRequest("The id parameter is required.");

            var category = await _categoryService.GetCategoryAsync(id);
            if (category == null)
                return NotFound();

            return category;
        }

        [HttpGet]
        public async Task<ActionResult<PaginatedListDto<CategoryDto>>> GetCategories(int pageIndex, int pageSize)
        {
            if (pageIndex < 1)
                return BadRequest("The pageIndex parameter must be at least 1.");
            if (pageSize < 1)
                return BadRequest("The pageSize parameter must be at least 1.");

            var categories = await _categoryService.GetCategoriesAsync(pageIndex, pageSize);
            return categories;
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> AddCategory(AddCategoryDto addCategoryDto)
        {
            if (ModelState.IsValid)
            {
                await _categoryService.AddCategoryAsync(addCategoryDto);
                return Ok();
            }
            return BadRequest(ModelState);
        }

        [HttpPut]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> EditCategory(EditCategoryDto editCategoryDto)
        {
            if (ModelState.IsValid)
            {
                await _categoryService.EditCategoryAsync(editCategoryDto);
                return Ok();
            }
            return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> DeleteCategory(string id)
        {
            if (id == null)
                return BadRequest("The id parameter is required.");

            await _categoryService.DeleteCategoryAsync(id);
            return Ok();
        }
    }
}