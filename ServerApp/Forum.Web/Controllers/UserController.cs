using System;
using System.Threading.Tasks;
using Forum.Bll.DTOs;
using Forum.Bll.Pagination;
using Forum.Bll.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Forum.Web.Controllers
{
    [ApiController]
    [Route("api/users")]
    [Authorize(Roles = "Administrator")]
    public class UserController : ControllerBase
    {
        private readonly IForumUserService _forumUserService;

        public UserController(IForumUserService forumUserService)
        {
            if (forumUserService == null)
                throw new ArgumentNullException(nameof(forumUserService));

            _forumUserService = forumUserService;
        }

        [HttpGet]
        public async Task<PaginatedListDto<ForumUserDto>> GetUsers(int pageIndex, int pageSize)
        {
            var users = await _forumUserService.GetUsersAsync(pageIndex, pageSize);
            return users;
        }

        [HttpGet("{id}")]
        public async Task<ForumUserDto> GetUser(string id)
        {
            var user = await _forumUserService.GetUser(id);
            return user;
        }
    }
}