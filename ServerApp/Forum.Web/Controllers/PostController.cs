using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Forum.Bll.DTOs;
using Forum.Bll.Pagination;
using Forum.Bll.Services.Interfaces;
using Forum.Web.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Forum.Web.Controllers
{
    [ApiController]
    [Route("api/posts")]
    [TypeFilter(typeof(LastActiveDateActionFilter))]
    public class PostController : ControllerBase
    {
        private readonly IPostService _postService;

        private readonly IAuthService _authService;

        public PostController(IPostService postService, IAuthService authService)
        {
            if (postService == null)
                throw new ArgumentNullException(nameof(postService));
            if (authService == null)
                throw new ArgumentNullException(nameof(authService));

            _postService = postService;
            _authService = authService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<PostDto>> GetPost(string id)
        {
            if (string.IsNullOrEmpty(id))
                return BadRequest("The id parameter is required.");

            var post = await _postService.GetPostAsync(id);
            if (post == null)
                return NotFound();

            return post;
        }

        [HttpGet]
        public async Task<ActionResult<PaginatedListDto<PostDto>>> GetPosts(
            int pageIndex, int pageSize, string conversationId, string content = null)
        {
            if (pageIndex < 1)
                return BadRequest("The pageIndex parameter must be at least 1.");
            if (pageSize < 1)
                return BadRequest("The pageSize parameter must be at least 1.");
            if (conversationId == null)
                return BadRequest("The conversationId parameter is required.");

            var posts = await _postService
                .GetPostsAsync(pageIndex, pageSize, conversationId, content);
            return posts;
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> AddPost(AddPostDto addPostDto)
        {
            if (ModelState.IsValid)
            {
                var userId = HttpContext.User?.FindFirst(ClaimTypes.NameIdentifier).Value;
                if (userId == null)
                    return StatusCode(401);
                await _postService.AddPostAsync(addPostDto, userId);
                return Ok();
            }
            return BadRequest(ModelState);
        }

        [HttpPut]
        [Authorize]
        public async Task<IActionResult> EditPost(EditPostDto editPostDto)
        {
            if (ModelState.IsValid)
            {
                var userId = HttpContext.User?.FindFirst(ClaimTypes.NameIdentifier).Value;
                if (userId == null)
                    return StatusCode(401);

                var post = await _postService.GetPostAsync(editPostDto.Id);
                if (post == null)
                    return NotFound();

                if (userId != post.ForumUserId &&
                    await _authService.IsInRole(userId, "Registred"))
                    return StatusCode(403);

                await _postService.EditPostAsync(editPostDto);
                return Ok();
            }
            return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeletePost(string id)
        {
            if (id == null)
                return BadRequest("The id parameter is required.");

            var userId = HttpContext.User?.FindFirst(ClaimTypes.NameIdentifier).Value;
            if (userId == null)
                return StatusCode(401);

            var post = await _postService.GetPostAsync(id);
            if (post == null)
                return NotFound();

            if (userId != post.ForumUserId &&
                await _authService.IsInRole(userId, "Registred"))
                return StatusCode(403);

            await _postService.DeletePostAsync(id);
            return Ok();
        }
    }
}