using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Forum.Bll.Services.Interfaces;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Forum.Web.Filters
{
    public class LastActiveDateActionFilter : IAsyncActionFilter
    {
        private readonly IForumUserService _forumUserService;

        public LastActiveDateActionFilter(IForumUserService forumUserService)
        {
            _forumUserService = forumUserService;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            await next();

            var userId = context.HttpContext.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            if (userId != null)
                await _forumUserService.ChangeLastActiveDate(userId, DateTime.UtcNow);
        }
    }
}
