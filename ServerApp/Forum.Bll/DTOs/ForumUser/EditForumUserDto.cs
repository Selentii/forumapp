using System;
using System.ComponentModel.DataAnnotations;

namespace Forum.Bll.DTOs
{
    public class EditForumUserDto
    {
        [Required]
        public string Id { get; set; }

        public int Reputation { get; set; }

        public DateTime LastActiveDate { get; set; }
    }
}