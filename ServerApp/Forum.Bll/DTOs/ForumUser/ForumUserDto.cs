using System;
using System.Collections.Generic;

namespace Forum.Bll.DTOs
{
    public class ForumUserDto
    {
        public string Id { get; set; }

        public string UserName { get; set; }

        public int Reputation { get; set; }

        public DateTime LastActiveDate { get; set; }

        public DateTime CreationDate { get; set; }

        public IEnumerable<ConversationDto> Conversations { get; set; }

        public IEnumerable<PostDto> Posts { get; set; }
    }
}