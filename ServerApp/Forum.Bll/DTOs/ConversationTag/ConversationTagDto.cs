using System.ComponentModel.DataAnnotations;

namespace Forum.Bll.DTOs
{
    public class ConversationTagDto
    {
        [Required]
        public string ConversationId { get; set; }

        [Required]
        public string TagId { get; set; }
    }
}