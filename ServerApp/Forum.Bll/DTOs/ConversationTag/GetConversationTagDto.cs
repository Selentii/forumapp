namespace Forum.Bll.DTOs
{
    public class GetConversationTagDto
    {
        public string ConversationId { get; set; }

        public string TagId { get; set; }

        public ConversationDto Conversation { get; set; }

        public TagDto Tag { get; set; }
    }
}