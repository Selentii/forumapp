using System.Collections.Generic;

namespace Forum.Bll.DTOs
{
    public class TagDto
    {
        public string Id { get; set; } 

        public string Name { get; set; }

        public IEnumerable<GetConversationTagDto> ConversationTags { get; set; }
    }
}