using System.ComponentModel.DataAnnotations;

namespace Forum.Bll.DTOs
{
    public class AddTagDto
    {
        [Required]
        [StringLength(30, MinimumLength = 2)]
        public string Name { get; set; }
    }
}