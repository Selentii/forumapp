using System.ComponentModel.DataAnnotations;

namespace Forum.Bll.DTOs
{
    public class EditTagDto
    {
        [Required]
        public string Id { get; set; } 

        [StringLength(30, MinimumLength = 2)]
        public string Name { get; set; }
    }
}