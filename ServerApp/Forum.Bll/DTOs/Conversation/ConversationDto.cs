using System;
using System.Collections.Generic;

namespace Forum.Bll.DTOs
{
    public class ConversationDto
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? EditingDate { get; set; }

        public string ForumUserId { get; set; }

        public string CategoryId { get; set; }

        public ForumUserDto ForumUser { get; set; }

        public CategoryDto Category { get; set; }

        public IEnumerable<PostDto> Posts { get; set; }

        public IEnumerable<GetConversationTagDto> ConversationTags { get; set; }
    }
}