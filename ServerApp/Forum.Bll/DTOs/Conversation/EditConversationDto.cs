using System.ComponentModel.DataAnnotations;

namespace Forum.Bll.DTOs
{
    public class EditConversationDto
    {
        [Required]
        public string Id { get; set; }

        [StringLength(100, MinimumLength = 2)]
        public string Title { get; set; }

        [StringLength(1000, MinimumLength = 2)]
        public string Description { get; set; }

        public string CategoryId { get; set; }
    }
}