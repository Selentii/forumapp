using System.ComponentModel.DataAnnotations;

namespace Forum.Bll.DTOs
{
    public class EditPostDto
    {
        [Required]
        public string Id { get; set; }

        [StringLength(1000, MinimumLength = 1)]
        public string Content { get; set; }
    }
}