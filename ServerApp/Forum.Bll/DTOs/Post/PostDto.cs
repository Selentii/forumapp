using System.Collections.Generic;

namespace Forum.Bll.DTOs
{
    public class PostDto
    {
        public string Id { get; set; }

        public string Content { get; set; }

        public string ReplyToId { get; set; }

        public string ForumUserId { get; set; }

        public string ConversationId { get; set; }

        public PostDto ReplyTo { get; set; }

        public IEnumerable<PostDto> Replies { get; set; }

        public ForumUserDto ForumUser { get; set; }

        public ConversationDto Conversation { get; set; }
    }
}