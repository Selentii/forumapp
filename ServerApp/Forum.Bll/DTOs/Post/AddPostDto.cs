using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.Bll.DTOs
{
    public class AddPostDto
    {
        [Required]
        [StringLength(1000, MinimumLength = 1)]
        public string Content { get; set; }

        public string ReplyToId { get; set; }

        [Required]
        public string ConversationId { get; set; }
    }
}