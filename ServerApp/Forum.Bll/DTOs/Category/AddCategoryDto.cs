using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.Bll.DTOs
{
    public class AddCategoryDto
    {
        [Required]
        [StringLength(30, MinimumLength = 2)]
        public string Name { get; set; } 

        [Required]
        public int Weight { get; set; }
    }
}