using System.Collections.Generic;

namespace Forum.Bll.DTOs
{
    public class CategoryDto
    {
        public string Id { get; set; }

        public string Name { get; set; } 

        public int Weight { get; set; }

        public IEnumerable<ConversationDto> Conversations { get; set; }
    }
}