using System.Collections.Generic;

namespace Forum.Bll.Pagination
{
    public class PaginatedListDto<T>
    {
        public int PageIndex { get; private set; }

        public int TotalPages { get; private set; }

        public int PageSize { get; set; }

        public int TotalItems { get; set; }

        public IEnumerable<T> Result { get; set; }
    }
}