using System.Threading.Tasks;
using Forum.Bll.DTOs;
using Forum.Bll.Pagination;

namespace Forum.Bll.Services.Interfaces
{
    public interface IPostService
    {
        Task<PostDto> GetPostAsync(string id);

        Task<PaginatedListDto<PostDto>> GetPostsAsync(
            int pageIndex, int pageSize, string conversationId, string content = null);
    
        Task AddPostAsync(AddPostDto addPostDto, string userId);

        Task EditPostAsync(EditPostDto editPostDto);

        Task DeletePostAsync(string id);
    }
}