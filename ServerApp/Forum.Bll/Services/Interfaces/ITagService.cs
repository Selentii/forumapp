using System.Threading.Tasks;
using Forum.Bll.DTOs;
using Forum.Bll.Pagination;

namespace Forum.Bll.Services.Interfaces
{
    public interface ITagService
    {
        Task<TagDto> GetTagAsync(string id);

        Task<PaginatedListDto<TagDto>> GetTagsAsync(int pageIndex, int pageSize);
    
        Task AddTagAsync(AddTagDto addTagDto);

        Task EditTagAsync(EditTagDto editTagDto);

        Task DeleteTagAsync(string id);
    }
}