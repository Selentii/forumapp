using System.Threading.Tasks;
using Forum.Bll.DTOs;

namespace Forum.Bll.Services.Interfaces
{
    public interface IAuthService
    {
        Task<ForumUserDto> RegisterAsync(RegisterDto registerDto);

        Task<string> LoginAsync(LoginDto loginDto);

        Task<bool> IsInRole(string userId, string role);
    }
}