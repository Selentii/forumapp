using System;
using System.Threading.Tasks;
using Forum.Bll.DTOs;
using Forum.Bll.Pagination;

namespace Forum.Bll.Services.Interfaces
{
    public interface IForumUserService
    {
        Task<PaginatedListDto<ForumUserDto>> GetUsersAsync(int pageIndex, int pageSize);

        Task<ForumUserDto> GetUser(string id);

        Task ChangeLastActiveDate(string id, DateTime date);

        Task ChangeReputation(string id, int reputation);
    }
}