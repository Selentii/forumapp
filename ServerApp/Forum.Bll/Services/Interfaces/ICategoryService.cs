using System.Threading.Tasks;
using Forum.Bll.DTOs;
using Forum.Bll.Pagination;

namespace Forum.Bll.Services.Interfaces
{
    public interface ICategoryService
    {
        Task<CategoryDto> GetCategoryAsync(string id);

        Task<PaginatedListDto<CategoryDto>> GetCategoriesAsync(int pageIndex, int pageSize);
    
        Task AddCategoryAsync(AddCategoryDto addCategoryDto);

        Task EditCategoryAsync(EditCategoryDto editCategoryDto);

        Task DeleteCategoryAsync(string id);
    }
}