using System.Collections.Generic;
using System.Threading.Tasks;
using Forum.Bll.DTOs;
using Forum.Bll.Pagination;

namespace Forum.Bll.Services.Interfaces
{
    public interface IConversationService
    {
        Task<ConversationDto> GetConversationAsync(string id);

        Task<PaginatedListDto<ConversationDto>> GetConversationsAsync(
            int pageIndex, int pageSize, string categoryId = null, string title = null, 
            IEnumerable<string> tagNames = null);

        Task AddConversationAsync(AddConversationDto addConversationDto, string userId);

        Task EditConversationAsync(EditConversationDto editConversationDto);

        Task DeleteConversationAsync(string id);

        Task TagConversationAsync(ConversationTagDto conversationTagDto);

        Task UnTagConversationAsync(ConversationTagDto conversationTagDto);
    }
}