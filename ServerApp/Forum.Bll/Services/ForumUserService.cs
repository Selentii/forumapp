using System;
using System.Threading.Tasks;
using AutoMapper;
using Forum.Bll.DTOs;
using Forum.Bll.Exceptions;
using Forum.Bll.Pagination;
using Forum.Bll.Services.Interfaces;
using Forum.Dal.Entities;
using Forum.Dal.UnitOfWorks.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace Forum.Bll.Services
{
    public class ForumUserService : IForumUserService
    {
        private readonly IUnitOfWork _db;

        private readonly UserManager<ForumUser> _userManager;

        private readonly IMapper _mapper;

        public ForumUserService(IUnitOfWork unitOfWork, UserManager<ForumUser> userManager,
            IMapper mapper)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException(nameof(unitOfWork));
            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));
            if (mapper == null)
                throw new ArgumentNullException(nameof(mapper));

            _db = unitOfWork;
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<PaginatedListDto<ForumUserDto>> GetUsersAsync(int pageIndex, int pageSize)
        {
            if (pageIndex < 1)
                throw new ArgumentOutOfRangeException(nameof(pageIndex));
            if (pageSize < 1)
                throw new ArgumentOutOfRangeException(nameof(pageSize));

            var users = await _db.ForumUsers.GetAllAsync(pageIndex, pageSize);
            return _mapper.Map<PaginatedListDto<ForumUserDto>>(users);
        }

        public async Task<ForumUserDto> GetUser(string id)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            var user = await _userManager.FindByIdAsync(id);
            return user != null ? _mapper.Map<ForumUserDto>(user) : null;
        }

        public async Task ChangeLastActiveDate(string id, DateTime date)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
                throw new QueryException("No user with specified primary key found");

            user.LastActiveDate = date;
            var result = await _userManager.UpdateAsync(user);

            if (!result.Succeeded)
                throw new IdentityException("Error occured while changing user's last active date",
                    result.Errors);
        }

        public async Task ChangeReputation(string id, int reputation)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
                throw new QueryException("No user with specified primary key found");

            user.Reputation = reputation;
            var result = await _userManager.UpdateAsync(user);

            if (!result.Succeeded)
                throw new IdentityException("Error occured while changing user's last active date",
                    result.Errors);
        }
    }
}