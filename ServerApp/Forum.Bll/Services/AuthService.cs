using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Forum.Bll.DTOs;
using Forum.Bll.Exceptions;
using Forum.Bll.Infrastructure;
using Forum.Bll.Services.Interfaces;
using Forum.Dal.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Forum.Bll.Services
{
    public class AuthService : IAuthService
    {
        private readonly UserManager<ForumUser> _userManager;

        private readonly AuthOptions _authOptions;

        private readonly IMapper _mapper;

        public AuthService(UserManager<ForumUser> userManager,
        IOptions<AuthOptions> authOptions, IMapper mapper)
        {
            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));
            if (authOptions == null)
                throw new ArgumentNullException(nameof(authOptions));
            if (mapper == null)
                throw new ArgumentNullException(nameof(mapper));

            _userManager = userManager;
            _authOptions = authOptions.Value;
            _mapper = mapper;
        }

        public async Task<ForumUserDto> RegisterAsync(RegisterDto registerDto)
        {
            if (registerDto == null)
                throw new ArgumentNullException(nameof(registerDto));

            var forumUser = _mapper.Map<ForumUser>(registerDto);

            var creationResult = await _userManager.CreateAsync(forumUser, registerDto.Password);
            if (!creationResult.Succeeded)
                throw new IdentityException("Error occured while creating user", creationResult.Errors);

            var roleAddingResult = await _userManager.AddToRoleAsync(forumUser, "Registred");
            if (!roleAddingResult.Succeeded)
                throw new IdentityException("Error occured while adding user to role", roleAddingResult.Errors);

            return _mapper.Map<ForumUserDto>(forumUser);
        }

        public async Task<string> LoginAsync(LoginDto loginDto)
        {
            if (loginDto == null)
                throw new ArgumentNullException(nameof(loginDto));

            var forumUser = await _userManager.FindByNameAsync(loginDto.UserName);
            if (forumUser == null)
                throw new QueryException("User with specified UserName not found");

            var result = await _userManager.CheckPasswordAsync(forumUser, loginDto.Password);
            if (!result)
                throw new IdentityException("Error occured while checking password");

            var token = await GetTokenAsync(forumUser);
            return token;
        }

        public async Task<bool> IsInRole(string userId, string role)
        {
            if (userId == null)
                throw new ArgumentNullException(nameof(userId));
            if (role == null)
                throw new ArgumentNullException(nameof(role));
                
            var forumUser = await _userManager.FindByIdAsync(userId);
            if (forumUser == null)
                throw new QueryException("User with specified UserName not found");

            return await _userManager.IsInRoleAsync(forumUser, role);
        }

        private async Task<string> GetTokenAsync(ForumUser forumUser)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, forumUser.Id),
                new Claim(ClaimTypes.Name, forumUser.UserName)
            };

            var roles = await _userManager.GetRolesAsync(forumUser);
            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var tokenKey = _authOptions.GetSymmetricSecurityKey();
            var credentials = new SigningCredentials(tokenKey, SecurityAlgorithms.HmacSha256);
            var now = DateTime.UtcNow;

            var securityTokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims, "Token"),
                SigningCredentials = credentials,
                NotBefore = now,
                Expires = now.AddMinutes(_authOptions.AccessExpiration),
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(securityTokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}