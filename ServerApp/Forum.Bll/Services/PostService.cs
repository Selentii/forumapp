using System;
using System.Threading.Tasks;
using AutoMapper;
using Forum.Bll.DTOs;
using Forum.Bll.Exceptions;
using Forum.Bll.Pagination;
using Forum.Bll.Services.Interfaces;
using Forum.Dal.Entities;
using Forum.Dal.UnitOfWorks.Interfaces;

namespace Forum.Bll.Services
{
    public class PostService : IPostService
    {
        private readonly IUnitOfWork _db;

        private readonly IMapper _mapper;

        public PostService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException(nameof(unitOfWork));
            if (mapper == null)
                throw new ArgumentNullException(nameof(mapper));

            _db = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PostDto> GetPostAsync(string id)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            var post = await _db.Posts.GetAsync(id);
            return post != null ? _mapper.Map<PostDto>(post) : null;
        }

        public async Task<PaginatedListDto<PostDto>> GetPostsAsync(
            int pageIndex, int pageSize, string conversationId, string content = null)
        {
            if (pageIndex < 1)
                throw new ArgumentOutOfRangeException(nameof(pageIndex));
            if (pageSize < 1)
                throw new ArgumentOutOfRangeException(nameof(pageSize));
            if (conversationId == null)
                throw new ArgumentNullException(nameof(conversationId));

            var contentLower = content?.ToLower();

            var posts = await _db.Posts.
                GetByPredicateAsync(p => p.ConversationId == conversationId &&
                    (contentLower == null || p.Content.ToLower().Contains(contentLower)), pageIndex, pageSize);
            return _mapper.Map<PaginatedListDto<PostDto>>(posts);
        }

        public async Task AddPostAsync(AddPostDto addPostDto, string userId)
        {
            if (addPostDto == null)
                throw new ArgumentNullException(nameof(addPostDto));
            if (userId == null)
                throw new ArgumentNullException(nameof(userId));

            if (addPostDto.ReplyToId != null)
            {
                var replyTo = await _db.Posts.GetAsync(addPostDto.ReplyToId);

                if (replyTo == null)
                    throw new QueryException("Reply to post is not found");

                if (replyTo.ConversationId != addPostDto.ConversationId)
                    throw new ConversationMismatchException(
                        "Added post is reply to the post from other conversation.", 
                        addPostDto.ConversationId, replyTo.ConversationId);
            }

            var post = _mapper.Map<Post>(addPostDto);
            post.ForumUserId = userId;

            var result = await _db.Posts.AddAsync(post);
            if (!result.Succeeded)
                throw new QueryException(result.ToString(), result.Errors);

            await _db.SaveChangesAsync();
        }

        public async Task EditPostAsync(EditPostDto editPostDto)
        {
            if (editPostDto == null)
                throw new ArgumentNullException(nameof(editPostDto));

            var post = _mapper.Map<Post>(editPostDto);

            var result = await _db.Posts.EditAsync(post);
            if (!result.Succeeded)
                throw new QueryException(result.ToString(), result.Errors);

            await _db.SaveChangesAsync();
        }

        public async Task DeletePostAsync(string id)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            var post = new Post { Id = id };

            var result = await _db.Posts.DeleteAsync(post);
            if (!result.Succeeded)
                throw new QueryException(result.ToString(), result.Errors);

            await _db.SaveChangesAsync();
        }
    }
}