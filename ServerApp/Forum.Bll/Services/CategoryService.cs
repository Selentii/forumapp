using System;
using System.Threading.Tasks;
using AutoMapper;
using Forum.Bll.DTOs;
using Forum.Bll.Exceptions;
using Forum.Bll.Pagination;
using Forum.Bll.Services.Interfaces;
using Forum.Dal.Entities;
using Forum.Dal.UnitOfWorks.Interfaces;

namespace Forum.Bll.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _db;

        private readonly IMapper _mapper;

        public CategoryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException(nameof(unitOfWork));
            if (mapper == null)
                throw new ArgumentNullException(nameof(mapper));

            _db = unitOfWork;
            _mapper = mapper;
        }

        public async Task<CategoryDto> GetCategoryAsync(string id)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            var category = await _db.Categories.GetAsync(id);
            return category != null ? _mapper.Map<CategoryDto>(category) : null;
        }

        public async Task<PaginatedListDto<CategoryDto>> GetCategoriesAsync(int pageIndex, int pageSize)
        {
            if (pageIndex < 1)
                throw new ArgumentOutOfRangeException(nameof(pageIndex));
            if (pageSize < 1)
                throw new ArgumentOutOfRangeException(nameof(pageSize));

            var categories = await _db.Categories.GetAllAsync(pageIndex, pageSize);
            return _mapper.Map<PaginatedListDto<CategoryDto>>(categories);
        }

        public async Task AddCategoryAsync(AddCategoryDto addCategoryDto)
        {
            if (addCategoryDto == null)
                throw new ArgumentNullException(nameof(addCategoryDto));

            var category = _mapper.Map<Category>(addCategoryDto);

            var result = await _db.Categories.AddAsync(category);
            if (!result.Succeeded)
                throw new QueryException(result.ToString(), result.Errors);
            
            await _db.SaveChangesAsync();
        }

        public async Task EditCategoryAsync(EditCategoryDto editCategoryDto)
        {
            if (editCategoryDto == null)
                throw new ArgumentNullException(nameof(editCategoryDto));

            var category = _mapper.Map<Category>(editCategoryDto);

            var result = await _db.Categories.EditAsync(category);
            if (!result.Succeeded)
                throw new QueryException(result.ToString(), result.Errors);

            await _db.SaveChangesAsync();
        }

        public async Task DeleteCategoryAsync(string id)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            var category = new Category { Id = id };

            var result = await _db.Categories.DeleteAsync(category);
            if (!result.Succeeded)
                throw new QueryException(result.ToString(), result.Errors);

            await _db.SaveChangesAsync();
        }
    }
}