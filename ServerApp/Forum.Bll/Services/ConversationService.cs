using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Forum.Bll.DTOs;
using Forum.Bll.Exceptions;
using Forum.Bll.Pagination;
using Forum.Bll.Services.Interfaces;
using Forum.Dal.Entities;
using Forum.Dal.Pagination;
using Forum.Dal.UnitOfWorks.Interfaces;

namespace Forum.Bll.Services
{
    public class ConversationService : IConversationService
    {
        private readonly IUnitOfWork _db;

        private readonly IMapper _mapper;

        public ConversationService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException(nameof(unitOfWork));
            if (mapper == null)
                throw new ArgumentNullException(nameof(mapper));

            _db = unitOfWork;
            _mapper = mapper;
        }
        
        public async Task<ConversationDto> GetConversationAsync(string id)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            var conversation = await _db.Conversations.GetAsync(id);
            return conversation != null ? _mapper.Map<ConversationDto>(conversation) : null;
        }

        public async Task<PaginatedListDto<ConversationDto>> GetConversationsAsync(
            int pageIndex, int pageSize, string categoryId = null, string title = null, 
            IEnumerable<string> tagNames = null)
        {
            if (pageIndex < 1)
                throw new ArgumentOutOfRangeException(nameof(pageIndex));
            if (pageSize < 1)
                throw new ArgumentOutOfRangeException(nameof(pageSize));

            var titleLower = title?.ToLower();
            var tagNamesLower = tagNames?.Select(t => t.ToLower());
            
            var conversations = await _db.Conversations
                .GetByPredicateAsync(c => (categoryId == null || c.CategoryId == categoryId) && 
                    (titleLower == null || c.Title.ToLower().Contains(titleLower)) &&
                    (tagNamesLower == null || c.ConversationTags.Count(ct => tagNamesLower.Contains(ct.Tag.Name.ToLower())) == tagNamesLower.Count()),
                    pageIndex, pageSize);
            return _mapper.Map<PaginatedListDto<ConversationDto>>(conversations);
        }

        public async Task AddConversationAsync(AddConversationDto addConversationDto, string userId)
        {
            if (addConversationDto == null)
                throw new ArgumentNullException(nameof(addConversationDto));
            if (userId == null)
                throw new ArgumentNullException(nameof(userId));

            var conversation = _mapper.Map<Conversation>(addConversationDto);
            conversation.ForumUserId = userId;

            var result = await _db.Conversations.AddAsync(conversation);
            if (!result.Succeeded)
                throw new QueryException(result.ToString(), result.Errors);

            await _db.SaveChangesAsync();
        }

        public async Task EditConversationAsync(EditConversationDto editConversationDto)
        {
            if (editConversationDto == null)
                throw new ArgumentNullException(nameof(editConversationDto));

            var conversation = _mapper.Map<Conversation>(editConversationDto);

            var result = await _db.Conversations.EditAsync(conversation);
            if (!result.Succeeded)
                throw new QueryException(result.ToString(), result.Errors);

            await _db.SaveChangesAsync();
        }

        public async Task DeleteConversationAsync(string id)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            var conversation = new Conversation { Id = id };

            var result = await _db.Conversations.DeleteAsync(conversation);
            if (!result.Succeeded)
                throw new QueryException(result.ToString(), result.Errors);

            await _db.SaveChangesAsync();
        }

        public async Task TagConversationAsync(ConversationTagDto conversationTagDto)
        {
            if (conversationTagDto == null)
                throw new ArgumentNullException(nameof(conversationTagDto));

            var conversationTag = _mapper.Map<ConversationTag>(conversationTagDto);

            var result = await _db.ConversationTags.AddAsync(conversationTag);
            if (!result.Succeeded)
                throw new QueryException(result.ToString(), result.Errors);

            await _db.SaveChangesAsync();
        }

        public async Task UnTagConversationAsync(ConversationTagDto conversationTagDto)
        {
            if (conversationTagDto == null)
                throw new ArgumentNullException(nameof(conversationTagDto));

            var conversationTag = _mapper.Map<ConversationTag>(conversationTagDto);

            var result = await _db.ConversationTags.DeleteAsync(conversationTag);
            if (!result.Succeeded)
                throw new QueryException(result.ToString(), result.Errors);

            await _db.SaveChangesAsync();
        }
    }
}