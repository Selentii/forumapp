using System;
using System.Threading.Tasks;
using AutoMapper;
using Forum.Bll.DTOs;
using Forum.Bll.Exceptions;
using Forum.Bll.Pagination;
using Forum.Bll.Services.Interfaces;
using Forum.Dal.Entities;
using Forum.Dal.UnitOfWorks.Interfaces;

namespace Forum.Bll.Services
{
    public class TagService : ITagService
    {
        private readonly IUnitOfWork _db;

        private readonly IMapper _mapper;

        public TagService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException(nameof(unitOfWork));
            if (mapper == null)
                throw new ArgumentNullException(nameof(mapper));

            _db = unitOfWork;
            _mapper = mapper;
        }

        public async Task<TagDto> GetTagAsync(string id)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            var tag = await _db.Tags.GetAsync(id);
            return tag != null ? _mapper.Map<TagDto>(tag) : null;
        }

        public async Task<PaginatedListDto<TagDto>> GetTagsAsync(int pageIndex, int pageSize)
        {
            if (pageIndex < 1)
                throw new ArgumentOutOfRangeException(nameof(pageIndex));
            if (pageSize < 1)
                throw new ArgumentOutOfRangeException(nameof(pageSize));

            var tags = await _db.Tags.GetAllAsync(pageIndex, pageSize);
            return _mapper.Map<PaginatedListDto<TagDto>>(tags);
        }

        public async Task AddTagAsync(AddTagDto addTagDto)
        {
            if (addTagDto == null)
                throw new ArgumentNullException(nameof(addTagDto));

            var tag = _mapper.Map<Tag>(addTagDto);

            var result = await _db.Tags.AddAsync(tag);
            if (!result.Succeeded)
                throw new QueryException(result.ToString(), result.Errors);

            await _db.SaveChangesAsync();
        }


        public async Task EditTagAsync(EditTagDto editTagDto)
        {
            if (editTagDto == null)
                throw new ArgumentNullException(nameof(editTagDto));

            var tag = _mapper.Map<Tag>(editTagDto);

            var result = await _db.Tags.EditAsync(tag);
            if (!result.Succeeded)
                throw new QueryException(result.ToString(), result.Errors);

            await _db.SaveChangesAsync();
        }

        public async Task DeleteTagAsync(string id)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            var tag = new Tag { Id = id };

            var result = await _db.Tags.DeleteAsync(tag);
            if (!result.Succeeded)
                throw new QueryException(result.ToString(), result.Errors);

            await _db.SaveChangesAsync();
        }
    }
}