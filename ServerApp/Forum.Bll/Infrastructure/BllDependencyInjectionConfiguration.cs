using Microsoft.Extensions.Configuration;
using Forum.Dal.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Forum.Bll.Services.Interfaces;
using Forum.Bll.Services;

namespace Forum.Bll.Infrastructure
{
    public static class BllDependencyInjectionConfiguration
    {
        public static IServiceCollection ConfigureBll(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.ConfigureDal(configuration);

            services.AddAutoMapper(typeof(BetweenDalAndBllMapperProfile));

            services.AddTransient<IDataSeeder, DataSeeder>();

            services.AddTransient<IAuthService, AuthService>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<IConversationService, ConversationService>();
            services.AddTransient<IForumUserService, ForumUserService>();
            services.AddTransient<IPostService, PostService>();
            services.AddTransient<ITagService, TagService>();

            return services;
        }
    }
}