using System.Threading.Tasks;

namespace Forum.Bll.Infrastructure
{
    public interface IDataSeeder
    {
        Task SeedDataAsync();
    }
}