using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Forum.Dal.Entities;
using Forum.Dal.UnitOfWorks.Interfaces;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;

namespace Forum.Bll.Infrastructure
{
    public class DataSeeder : IDataSeeder
    {
        private readonly RoleManager<IdentityRole> _roleManager;

        private readonly UserManager<ForumUser> _userManager;

        private readonly IUnitOfWork _db;

        private readonly string _currentDirectory;

        private const string JsonFilesPath = "../Forum.Bll/SeedData";

        public DataSeeder(RoleManager<IdentityRole> roleManager,
            UserManager<ForumUser> userManager, IUnitOfWork unitOfWork)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _db = unitOfWork;
            _currentDirectory = Directory.GetCurrentDirectory();
        }

        public async Task SeedDataAsync()
        {
            await SeedRolesDataAsync();
            await SeedUsersDataAsync();

            await SeedCategoriesDataAsync();
            await SeedConversationsDataAsync();
            await SeedPostsDataAsync();
            await SeedTagsDataAsync();
            await SeedConversationTagsDataAsync();
        }

        private async Task SeedRolesDataAsync()
        {
            if (!_roleManager.Roles.Any())
            {
                var fullPath = GetFullPath("Roles.json");

                List<IdentityRole> roles;

                using (var reader = new StreamReader(fullPath))
                {
                    string rolesJson = await reader.ReadToEndAsync();
                    roles = JsonConvert.DeserializeObject<List<IdentityRole>>(rolesJson);
                }

                for (int i = 0; i < roles.Count; i++)
                {
                    await _roleManager.CreateAsync(roles[i]);
                }
            }
        }

        private async Task SeedUsersDataAsync()
        {
            if (!_userManager.Users.Any())
            {
                var fullPath = GetFullPath("Users.json");
                List<ForumUser> users;

                using (var reader = new StreamReader(fullPath))
                {
                    string usersJson = await reader.ReadToEndAsync();
                    users = JsonConvert.DeserializeObject<List<ForumUser>>(usersJson);
                }

                await _userManager.CreateAsync(users[0], "MySecret123$");
                await _userManager.AddToRoleAsync(users[0], "Administrator");

                await _userManager.CreateAsync(users[1], "MySecret123$");
                await _userManager.AddToRoleAsync(users[1], "Moderator");

                for (int i = 2; i < users.Count; i++)
                {
                    await _userManager.CreateAsync(users[i], "MySecret123$");
                    await _userManager.AddToRoleAsync(users[i], "Registred");
                }
            }
        }

        private async Task SeedCategoriesDataAsync()
        {
            if ((await _db.Categories.GetAllAsync(1, 1)).TotalItems == 0)
            {
                var fullPath = GetFullPath("Categories.json");
                List<Category> categories;

                using (var reader = new StreamReader(fullPath))
                {
                    string categoriesJson = await reader.ReadToEndAsync();
                    categories = JsonConvert.DeserializeObject<List<Category>>(categoriesJson);
                }

                foreach (var category in categories)
                {
                    await _db.Categories.AddAsync(category);
                }
                await _db.SaveChangesAsync();
            }
        }

        private async Task SeedConversationsDataAsync()
        {
            if ((await _db.Conversations.GetAllAsync(1, 1)).TotalItems == 0)
            {
                var fullPath = GetFullPath("Conversations.json");
                List<Conversation> conversations;

                using (var reader = new StreamReader(fullPath))
                {
                    string conversationsJson = await reader.ReadToEndAsync();
                    conversations = JsonConvert.DeserializeObject<List<Conversation>>(conversationsJson);
                }

                foreach (var conversation in conversations)
                {
                    await _db.Conversations.AddAsync(conversation);
                }
                await _db.SaveChangesAsync();
            }
        }

        private async Task SeedPostsDataAsync()
        {
            if ((await _db.Posts.GetAllAsync(1, 1)).TotalItems == 0)
            {
                var fullPath = GetFullPath("Posts.json");
                List<Post> posts;

                using (var reader = new StreamReader(fullPath))
                {
                    string postsJson = await reader.ReadToEndAsync();
                    posts = JsonConvert.DeserializeObject<List<Post>>(postsJson);
                }

                foreach (var post in posts)
                {
                    await _db.Posts.AddAsync(post);
                }
                await _db.SaveChangesAsync();
            }
        }

        private async Task SeedTagsDataAsync()
        {
            if ((await _db.Tags.GetAllAsync(1, 1)).TotalItems == 0)
            {
                var fullPath = GetFullPath("Tags.json");
                List<Tag> tags;

                using (var reader = new StreamReader(fullPath))
                {
                    string tagsJson = await reader.ReadToEndAsync();
                    tags = JsonConvert.DeserializeObject<List<Tag>>(tagsJson);
                }

                foreach (var tag in tags)
                {
                    await _db.Tags.AddAsync(tag);
                }
                await _db.SaveChangesAsync();
            }
        }

        private async Task SeedConversationTagsDataAsync()
        {
            var fullPath = GetFullPath("ConversationTags.json");
            List<ConversationTag> conversationTags;

            using (var reader = new StreamReader(fullPath))
            {
                string conversationTagsJson = await reader.ReadToEndAsync();
                conversationTags = JsonConvert.DeserializeObject<List<ConversationTag>>(conversationTagsJson);
            }

            foreach (var conversationTag in conversationTags)
            {
                await _db.ConversationTags.AddAsync(conversationTag);
            }
            await _db.SaveChangesAsync();
        }

        private string GetFullPath(string fileName)
        {
            var fullPath = Path.Combine(_currentDirectory, JsonFilesPath, fileName);
            return fullPath;
        }
    }
}