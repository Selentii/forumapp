using System;
using AutoMapper;
using Forum.Bll.DTOs;
using Forum.Bll.Pagination;
using Forum.Dal.Entities;
using Forum.Dal.Pagination;

namespace Forum.Bll.Infrastructure
{
    public class BetweenDalAndBllMapperProfile : Profile
    {
        public BetweenDalAndBllMapperProfile()
        {
            CreatePaginatedListMap();

            CreateCategoryMap();
            CreateConversationMap();
            CreateConversationTagMap();
            CreateForumUserMap();
            CreatePostMap();
            CreateTagMap();
        }

        private void CreatePaginatedListMap()
        {
            CreateMap(typeof(PaginatedList<>), typeof(PaginatedListDto<>)).ReverseMap();
        }

        private void CreateCategoryMap()
        {
            CreateMap<Category, CategoryDto>().ReverseMap();
            CreateMap<EditCategoryDto, Category>();
            CreateMap<AddCategoryDto, Category>();
        }

        private void CreateConversationMap()
        {
            CreateMap<Conversation, ConversationDto>().ReverseMap();

            CreateMap<EditConversationDto, Conversation>()
                .ForMember(c => c.EditingDate, options =>
                    options.MapFrom(source => DateTime.UtcNow));

            CreateMap<AddConversationDto, Conversation>()
                .ForMember(c => c.CreationDate, options =>
                    options.MapFrom(source => DateTime.UtcNow));
        }

        private void CreateConversationTagMap()
        {
            CreateMap<ConversationTagDto, ConversationTag>();
            CreateMap<ConversationTag, GetConversationTagDto>();
        }

        private void CreateForumUserMap()
        {
            CreateMap<ForumUser, ForumUserDto>().ReverseMap();
            CreateMap<EditForumUserDto, ForumUser>();
            CreateMap<LoginDto, ForumUser>();

            CreateMap<RegisterDto, ForumUser>()
                .ForMember(u => u.CreationDate, options =>
                    options.MapFrom(source => DateTime.UtcNow))
                .ForMember(u => u.LastActiveDate, options =>
                    options.MapFrom(source => DateTime.UtcNow));
        }

        private void CreatePostMap()
        {
            CreateMap<Post, PostDto>().ReverseMap();

            CreateMap<EditPostDto, Post>()
                .ForMember(p => p.EditingDate, options =>
                    options.MapFrom(source => DateTime.UtcNow));

            CreateMap<AddPostDto, Post>()
                .ForMember(p => p.CreationDate, options =>
                    options.MapFrom(source => DateTime.UtcNow));
        }

        private void CreateTagMap()
        {
            CreateMap<Tag, TagDto>().ReverseMap();
            CreateMap<EditTagDto, Tag>();
            CreateMap<AddTagDto, Tag>();
        }
    }
}