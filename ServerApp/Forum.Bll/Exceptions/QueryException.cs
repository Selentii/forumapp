using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Forum.Dal.Infrastructure;

namespace Forum.Bll.Exceptions
{
    [Serializable]
    public class QueryException : Exception
    {
        public IEnumerable<QueryError> QueryErrors { get; }

        public QueryException() { }

        public QueryException(string message) : base(message) { }

        public QueryException(string message, Exception inner)
            : base(message, inner) { }

        public QueryException(string message, IEnumerable<QueryError> queryErrors)
            : base(message)
        {
            QueryErrors = queryErrors;
        }

        protected QueryException(SerializationInfo info,
            StreamingContext context) : base(info, context) { }
    }
}