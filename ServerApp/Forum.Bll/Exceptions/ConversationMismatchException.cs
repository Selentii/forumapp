using System;
using System.Runtime.Serialization;

namespace Forum.Bll.Exceptions
{
    [Serializable]
    public class ConversationMismatchException : Exception
    {
        public string ExpectedId { get; }

        public string ActualId { get; }

        public ConversationMismatchException() { }

        public ConversationMismatchException(string message) : base(message) { }

        public ConversationMismatchException(string message, Exception inner)
            : base(message, inner) { }

        public ConversationMismatchException(string message, string expectedId, string actualId)
            : base(message)
        {
            ExpectedId = expectedId;
            ActualId = actualId;
        }

        protected ConversationMismatchException(SerializationInfo info,
            StreamingContext context) : base(info, context) { }
    }
}